FROM ubuntu:xenial
MAINTAINER Tom Butler

RUN apt-get update
# Python
RUN apt-get install -y --no-install-recommends apt-utils
RUN apt-get install -y python3-pip wget unzip jq curl git
RUN pip3 install awscli boto3
# Ruby, and Bundler
RUN apt-get install -y ruby2.3 ruby2.3-dev
RUN gem install bundler

# Git
RUN apt-get install -y git

# Terraform
# Download Terraform - most recent version
RUN curl -o terraform.zip $(echo "https://releases.hashicorp.com/terraform/$(curl -s https://checkpoint-api.hashicorp.com/v1/check/terraform | jq -r -M '.current_version')/terraform_$(curl -s https://checkpoint-api.hashicorp.com/v1/check/terraform | jq -r -M '.current_version')_linux_amd64.zip")

# Unzip and install
RUN unzip terraform.zip
RUN mv terraform /usr/local/bin/terraform
RUN terraform -v 